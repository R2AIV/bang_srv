#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

char *GetCurrentTimeString();

char *log_str(char *msg)
{
	char time_string[1024];
	char log_string[1024];
	char hostname[100];

	FILE *logfile = fopen("srv.log", "a+");

	gethostname(hostname, sizeof(hostname));

	sprintf(log_string, "%s %s -> %s", GetCurrentTimeString(), hostname, msg);
	fprintf(logfile, log_string);
	fclose(logfile);
}

char *GetCurrentTimeString()
{
	time_t now;
	struct tm *ct;
	static char TimeString[100];

	now = time(NULL);
	ct = localtime(&now);

	uint8_t year = ct->tm_year - 100;
	uint8_t month = ct->tm_mon + 1;
	uint8_t day = ct->tm_mday;
	uint8_t hour = ct->tm_hour;
	uint8_t min = ct->tm_min;
	uint8_t sec = ct->tm_sec;

	sprintf(TimeString, "%.2d.%.2d.%.2d %.2d:%.2d:%.2d", day, month, year, hour, min, sec);

	return TimeString;
}

void print_log()
{
	// printf("%s\r\n", log_str());
}

