#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <arpa/inet.h>

#include <pthread.h>

#include "log.h"

#define RESPONSE "HTTP/1.1 200 OK\r\n\
Content-type: text/html;charset=utf-8\r\n\
Connection: close\r\n\
Server: BangGangSrv 0.1\r\n"

static char err_msg[1024];

static int resp_count = 0;

void *cli_thread(void *cli_sock)
{
	int *sock = cli_sock;
	char out_buff[10240];
	char in_buff[1024];
	int html_fd = 0;

	log_str("client connected\r\n");

	resp_count++;

	html_fd = open("page.html",O_RDONLY);
	read(html_fd, out_buff, sizeof(out_buff));
	close(html_fd);

	write(*sock, out_buff, strlen(out_buff));

	sprintf(out_buff, "<br><i><h4>Сыграно игр: %d!</h4></i><br>", resp_count);
	write(*sock, out_buff, strlen(out_buff));

	shutdown(*sock, SHUT_RDWR);
	close(*sock);

	log_str("connection closed!\r\n");
}

int tcp_srv(int port)
{
	struct sockaddr_in srv_addr;
	int srv_sock = 0;
	int stat = 0;

	memset((void *)&srv_addr,0,sizeof(srv_addr));
	srv_addr.sin_addr.s_addr = INADDR_ANY;
	srv_addr.sin_port = htons(port);
	srv_addr.sin_family = AF_INET;

	srv_sock = socket(AF_INET, SOCK_STREAM, 0);
	stat = bind(srv_sock, (struct sockaddr *)&srv_addr, sizeof(srv_addr));
	if(stat == -1)
	{
		sprintf(err_msg, "Socket bind error!\r\n");
		return -1;
	}
	listen(srv_sock, 1048);
	return srv_sock;
}

int main(void)
{
	int srv_sock = 0;
	int cli_sock = 0;
	pid_t pid = 0;
	char srv_buff[1024];
	char srv_in_buff[1024];
	pthread_t cli_thr;

	pid = fork();
	if(pid != 0) 
	{	printf("Server daemonized. PID: %d\r\n", pid);
		return 0;
	};

	srv_sock = tcp_srv(6767);
	if(srv_sock == -1)
	{
		printf(err_msg);
		return -1;
	}

	log_str("Server started and ready!\r\n");

	while(1)
	{
		cli_sock = accept(srv_sock, NULL, NULL);

		pthread_create(&cli_thr, NULL, cli_thread, (void *)&cli_sock);
		pthread_join(cli_thr, NULL);
	}

	return 0;
};
